import React from "react";
import "./App.css";
import NavBar from "./components/nav";
function App() {
  return (
    <div className="App container-fluid">
      <NavBar />
    </div>
  );
}

export default App;
