import { combineReducers } from "redux";

import getEntriesReducer from "./getEntriesReducer";

const RootReducer = combineReducers({
    getEntries: getEntriesReducer
});
export default RootReducer;


