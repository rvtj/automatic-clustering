const initialState = {
    categorical: "------",
    numerical: "",
    data: [],
    chartDataUpdate: false,
    firstMeanBarCharts: [],
    firstSDBarCharts: [],
    secondMeanBarCharts: [],
    secondSDBarCharts: [],
    firstScatterPlot: [],
    secondScatterPlot: [],
    firstLineCharts: [],
    secondLineCharts: []
};


const GetEntriesReducer = (state = initialState, action) => {
    let newState = { ...state };
    const ap = action.payload;
    switch (action.type) {
        case "UPDATE_VALUE":
            return {
                ...state,
                categorical: ap.categorical,
                numerical: ap.numerical,
                data: ap.data
            };
        case "UPDATE_CHART_DATA":
            return {
                ...state,
                chartDataUpdate: true,
                firstMeanBarCharts: ap.firstMeanBarCharts,
                firstSDBarCharts: ap.firstSDBarCharts,
                secondMeanBarCharts: ap.secondMeanBarCharts,
                secondSDBarCharts: ap.secondSDBarCharts,
                firstScatterPlot: ap.firstScatterPlot,
                secondScatterPlot: ap.secondScatterPlot,
                firstLineCharts: ap.firstLineCharts,
                secondLineCharts: ap.secondLineCharts


            };
        case "RESET_CHART_DATA":
            ////console.log('reset chart data')
            return {
                ...state,
                chartDataUpdate: false
            }
        default:
    }
    return newState;
};

export default GetEntriesReducer;
