import jValue from '../../json/data.json';
import jData from '../../json/data1.json';
import axios from 'axios'


export const getCategories = formData => {
    console.log(jValue)
    return dispatch => {

        //dev
        dispatch({
            type: "UPDATE_VALUE",
            payload: jValue
        });

        //prod

        // axios.post('/upload', formData, {
        //     headers: {
        //         'Content-Type': 'multipart/form-data'
        //     }
        // })
        //     .then(function (response) {
        //         console.log(response)
        //         dispatch({
        //             type: "UPDATE_VALUE",
        //             payload: response
        //         });

        //     })
        //     .catch(function (error) {
        //         //console.log(error);
        //     });
    };

};



export const getDataForCharts = formData => {
    console.log(formData)
    return dispatch => {

        //dev
        dispatch({
            type: "UPDATE_CHART_DATA",
            payload: computedResponse(jData.data)
        });

        //prod
        // axios.post('/model_running', formData, {
        //     headers: {
        //         'Content-Type': 'multipart/form-data'
        //     }
        // })
        //     .then(function (response) {
        //         console.log(response)
        //         dispatch({
        //             type: "UPDATE_CHART_DATA",
        //             payload: response
        //         });

        //     })
        //     .catch(function (error) {
        //         //console.log(error);
        //     });
    };
};
const lineChartData = (data, label) => {
    var color = ['red', 'green', 'blue', 'grey', 'black', 'yellow']
    const objArr = { [label]: [{}, {}, {}] }
    console.log(objArr)
    const temps = Object.keys(data).map((a, index) => {
        // objArr[label[index]] = {}
        objArr[label][index].name = `Team ${index}`
        objArr[label][index].x = []
        objArr[label][index].y = []
        objArr[label][index].type = "line"
        objArr[label][index].marker = { color: color[index] }
        data[a].map((b, indexs) => {
            objArr[label][index].y.push(b);
            objArr[label][index].x.push(indexs);
        })
        return false;
    })
    // console.log(objArr)
    return objArr;
}

const barChartFormatedData = (data) => {
    var color = ['red', 'green', 'blue', 'grey', 'black', 'yellow']
    const temp = { type: "bar", x: [], y: [], marker: { color: [] } }
    const temps = Object.keys(data).map((a, index) => {
        temp.x.push(a);
        temp.y.push(data[a]);
        temp.marker.color.push(color[index]);
        return false;
    })
    return temp;
}
const scatterChartFormatedData = (data) => {
    var color = ['red', 'green', 'blue', 'grey', 'black', 'yellow']
    const objArr = [{}, {}, {}];
    const temps = Object.keys(data).map((a, index) => {
        objArr[index].name = `Team ${index}`
        objArr[index].x = []
        objArr[index].y = []
        objArr[index].type = "scatter"
        objArr[index].mode = "markers"
        objArr[index].marker = { color: color[index] }
        data[a].map((b) => {
            objArr[index].x.push(b.x);
            objArr[index].y.push(b.y);
        })
        return false;
    })
    // //console.log(objArr)
    return objArr;
}

const finalResult = (chartType, data) => {
    switch (chartType) {
        case "bar":
            return Object.keys(data).map(a => {
                return barChartFormatedData(data[a])
            })
        case "scatter":
            // return Object.keys(data).map(a => {
            return scatterChartFormatedData(data)
        //})
        case "line":
            return Object.keys(data).map(a => {
                return lineChartData(data[a], a)
            })
    }
}


const computedResponse = (data) => {
    var tempRespose = {
        firstMeanBarCharts: finalResult("bar", data.First_Means),
        firstSDBarCharts: finalResult("bar", data.First_Standard_Deviation),
        secondMeanBarCharts: finalResult("bar", data.Second_Means),
        secondSDBarCharts: finalResult("bar", data.Second_Standard_Deviation),
        firstScatterPlot: finalResult("scatter", data.First_Scatter_plot),
        secondScatterPlot: finalResult("scatter", data.Second_Scatter_plot),
        firstLineCharts: finalResult("line", data.First_Density_Plot),
        secondLineCharts: finalResult("line", data.Second_Density_Plot),
    }
    return tempRespose;
}







