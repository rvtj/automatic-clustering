import React, { Component } from 'react'
import SelectBox from './selectBox'
import { connect } from 'react-redux'
import * as ActionCreator from '../stores/action/actions'
import "./homeComponent.scss"

class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Numerical: [],
            Categorical: []
        }
        this.onselection = this.onselection.bind(this);
        this.myFunction = this.myFunction.bind(this);
    }
    myFunction() {
        console.log(this.state)
        const formData = new FormData()
        formData.append("Numerical", this.state.Numerical)
        formData.append("Categorical", this.state.Categorical)
        this.props.getDataForCharts(formData)
    }
    onselection(a, b, c) {
        const temp = [...this.state[c]]
        if (b === "checked") {
            temp.push(a);
        } else {
            temp.splice(temp.indexOf(a), 1);
        }
        this.setState({
            ...this.state,
            [c]: temp
        })
    }
    render() {
        // ////console.log(this.props.data);
        const title = Object.keys(this.props.data[0])
        const test = this.props.data.map((a, index) => {

            ////console.log(a)
        })
        return (
            <>
                <div>
                    <div className="container">
                        <div className="row">

                            <div className="col-sm-6">
                                <SelectBox data={this.props.numerical} onchecked={this.onselection} title="Numerical"></SelectBox>
                            </div>
                            <div className="col-sm-6">
                                <SelectBox data={this.props.categorical} onchecked={this.onselection} title="Categorical"></SelectBox>
                            </div>
                        </div>

                    </div>
                    <div className="col-sm-12">
                        <table className="table">
                            <tbody>
                                <tr>
                                    {title.map((a, index) => {
                                        return (
                                            <th key={index}>
                                                {a}
                                            </th>
                                        )
                                    })}
                                </tr>
                                {this.props.data.map((a, index) => {
                                    return (
                                        <tr key={index}>
                                            {
                                                Object.keys(a).map((b, index) => {
                                                    return (<td key={index}>{a[b]}</td>)
                                                })
                                            }
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <button className="btn btn-primary" disabled={(!this.state.Categorical.length < 1 && !this.state.Numerical.length < 1) ? false : true} onClick={this.myFunction} >Submit</button>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    }
}

const dispatchProps = (dispatch) => {
    return {
        getDataForCharts: (data) => { dispatch(ActionCreator.getDataForCharts(data)) },
    }

}
export default connect(mapStateToProps, dispatchProps)(HomeComponent);