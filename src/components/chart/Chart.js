import React from 'react';
import Plot from 'react-plotly.js';

class Chart extends React.Component {
    render() {
        console.log(this.props.title, '----------')
        return (
            <>
                < Plot
                    data={this.props.data}
                    layout={{ title: this.props.title, width: this.props.width || 500, height: 500, yaxis: { title: this.props.titleY }, xaxis: { title: this.props.titleX } }}
                />

            </>
        )
    }
}



export default Chart; 