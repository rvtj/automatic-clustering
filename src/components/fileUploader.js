import React, { Component } from "react";
import { connect } from 'react-redux'
import * as ActionCreator from '../stores/action/actions'
import "./fileUploader.scss";



class FileUploader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      disabledSubmit: false
    };
    this.uploadFile = this.uploadFile.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
  }
  uploadFile(e) {
    this.setState({
      selectedFile: e.target.files[0],
      disabledSubmit: false
    });
  }
  onClickHandler = (e) => {
    e.preventDefault();
    this.setState({
      ...this.state,
      disabledSubmit: true
    });
    const formData = new FormData()
    formData.append('file', this.state.selectedFile)
    this.props.getCategories(formData)
    this.props.resetChartData()

  };
  render() {

    const fileUploader = {
      marginTop: "70px",
      marginBottom: "70px"
    };
    return (
      <>
        <div className="col-sm-12" style={fileUploader}>
          <input type="file" onChange={this.uploadFile} />

          <button className="btn btn-primary" disabled={this.state.disabledSubmit} onClick={this.onClickHandler}>upload file</button>

        </div>
      </>
    );
  }
}

const updateStatetoProps = (state) => {
  return {

  }
}
const dispatchProps = (dispatch) => {
  return {
    getCategories: (val) => { dispatch(ActionCreator.getCategories(val)) },
    resetChartData: () => {
      dispatch({ type: "RESET_CHART_DATA" })
    }
  }
}

export default connect(updateStatetoProps, dispatchProps)(FileUploader);