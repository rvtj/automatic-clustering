import React, { Component } from "react";
import Chart from "../chart/Chart";
import { connect } from "react-redux";
import "./charts.scss";
class SecondResult extends Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-sm-12 charts"><h3>Second Result</h3></div>
                </div>
                <div className='row'>
                    <div className="col-sm-12">
                        <Chart data={this.props.secondScatterPlot} title={` Second Scatter Plot`} className="firstScatterPlot" width="1000" />
                    </div>
                    <div className="col-sm-6">
                        {this.props.secondMeanBarCharts.map((a, index) => {
                            return <Chart key={index} data={[a]} title={`Mean of various features in cluster ${index + 1}`} className="firstScatterPlot" />

                        })}
                    </div>
                    <div className="col-sm-6">
                        {this.props.secondSDBarCharts.map((a, index) => {
                            return <Chart key={index} data={[a]} title={`SD of various features in cluster ${index + 1}`} className="firstScatterPlot" />

                        })}
                    </div>
                    <div className="col-sm-12">
                        {this.props.secondLineCharts.map((a, index) => {
                            return Object.keys(a).map((b, index) => {
                                const tit = b.charAt(0).toUpperCase() + b.slice(1).replace("_", " ")
                                return <Chart data={a[b]} key={index} title={tit} className="firstScatterPlot" />
                            })
                        })}
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    ////console.log(state.getEntries);
    return {
        secondMeanBarCharts: state.getEntries.secondMeanBarCharts,
        secondSDBarCharts: state.getEntries.secondSDBarCharts,
        secondScatterPlot: state.getEntries.secondScatterPlot,
        secondLineCharts: state.getEntries.secondLineCharts,
    };
};
const dispatchToProps = dispatch => {
    return {

    };
};
export default connect(
    mapStateToProps,
    dispatchToProps
)(SecondResult);
