import React, { Component } from "react";
import Chart from "../chart/Chart";
import { connect } from "react-redux";
import "./charts.scss";
import "./firstResult.scss"
class FirstResult extends Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-sm-12 charts"><h3>First Result</h3></div>
                </div>
                <div className='row'>
                    <div className="col-sm-12">
                        <Chart data={this.props.firstScatterPlot} title={`First Scatter Plot`} className="firstScatterPlot" width="1000" />
                    </div>
                    <div className="col-sm-6">
                        {this.props.firstMeanBarCharts.map((a, index) => {
                            return <Chart key={index} data={[a]} title={`Mean of various features in cluster ${index + 1}`} className="firstScatterPlot" />
                        })}
                    </div>
                    <div className="col-sm-6">
                        {this.props.firstSDBarCharts.map((a, index) => {
                            return <Chart key={index} data={[a]} title={`SD of various features in cluster ${index + 1}`} className="firstScatterPlot" />

                        })}
                    </div>
                    <div className="col-sm-12">
                        {this.props.firstLineCharts.map((a, index) => {
                            return Object.keys(a).map((b, index) => {
                                const tit = b.charAt(0).toUpperCase() + b.slice(1).replace("_", " ")
                                return <Chart data={a[b]} key={index} title={tit} className="firstScatterPlot" />
                            })
                        })}
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        firstMeanBarCharts: state.getEntries.firstMeanBarCharts,
        firstSDBarCharts: state.getEntries.firstSDBarCharts,
        firstScatterPlot: state.getEntries.firstScatterPlot,
        firstLineCharts: state.getEntries.firstLineCharts,
    };
};
const dispatchToProps = dispatch => {
    return {


    };
};
export default connect(
    mapStateToProps,
    dispatchToProps
)(FirstResult);
