import React, { Component } from "react";
import { BrowserRouter, NavLink, Route } from "react-router-dom";
import "./nav.scss";
import logo from "../image/logo.png";
import SelectComponent from "./selectBox";
import FileUploader from "./fileUploader";
import { connect } from "react-redux";
import * as ActionCreator from "../stores/action/actions";
import HomeComponent from './homeComponent';
import FirstResult from "./pages/firstResult";
import SecondResult from "./pages/secondResult";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSelector: false,
      numerical: "",
      tabIndex: 'tab1',
      showTabs: false,
      initRun: true
    }
    this.updateTabIndex = this.updateTabIndex.bind(this)
  }
  static getDerivedStateFromProps(props, state) {
    if (state.numerical !== props.numerical) {
      return {
        ...state,
        initRun: false,
        showSelector: true,
        numerical: props.numerical
      }
    }
    if (props.chartDataUpdate) {
      return {
        ...state,
        showSelector: false,
        showTabs: true
      }
    } else if (!props.chartDataUpdate) {
      return {
        ...state,
        showTabs: false,
        showSelector: true
      }
    }
    return null;
  }

  updateTabIndex(e) {
    this.setState({
      ...this.state,
      tabIndex: e.target.id
    })
    ////console.log(e.target.id, this.state.showTabs)
  }
  render() {
    console.log(this.state.initRun)
    return (
      <>
        <div className="container-fluid">
          <div id="top-nav" className="row">
            <div className="logo">
              <img src={logo} alt="" />
              <span>Automatic Clustering</span>
            </div>
            <div className="search" />
          </div>
          <div>
            {this.props.chartDataUpdate ? <ul>
              <li className="tabs" id='tab1' onClick={this.updateTabIndex}>First Result</li>
              <li className="tabs" id='tab2' onClick={this.updateTabIndex}>Second Result</li>
            </ul> : ""}

          </div>
          <div className="row">
            <FileUploader />
          </div>
          <div>
            {this.state.showSelector && !this.state.initRun ? <HomeComponent categorical={this.props.categorical} numerical={this.props.numerical} data={this.props.data}></HomeComponent> : ""}
          </div>
          {this.state.tabIndex == "tab1" && this.state.showTabs ? <FirstResult></FirstResult> : ""}
          {this.state.tabIndex == "tab2" && this.state.showTabs ? <SecondResult></SecondResult> : ""}
        </div>
      </>
    );
  }
}
const mapPropsToState = state => {
  console.log(state)
  return {
    categorical: state.getEntries.categorical,
    numerical: state.getEntries.numerical,
    chartDataUpdate: state.getEntries.chartDataUpdate,
    data: state.getEntries.data
  };
};
const mapDispachToProps = dispatch => {
  return {
  };
};
export default connect(
  mapPropsToState,
  mapDispachToProps
)(NavBar);


