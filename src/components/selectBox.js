import React, { Component } from "react";
import "./selectBox.scss";


class Selection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedArr: []
    };
    this.checkWhat = this.checkWhat.bind(this);
    this.fetchData = this.fetchData.bind(this);
  }

  checkWhat(e) {
    const temp = [...this.state.checkedArr];
    if (e.target.checked) {
      temp.push(e.target.value);
      this.props.onchecked(e.target.value, "checked", this.props.title);
    } else {
      this.props.onchecked(e.target.value, "unchecked", this.props.title);
      temp.splice(temp.indexOf(e.target.value), 1);
    }
    this.setState({
      ...this.state,
      checkedArr: temp
    });
  }
  fetchData() {
    const obj = {
      data: [this.state.checkedArr]
    };
    ////console.log(obj);
  }
  render() {
    const rows = {
      display: "inline-block",
      margin: "3% 3%"
    };
    const ulStyle = {
      listStyle: "none"
    };
    return (
      <>
        <div>
          <div className="details">
            <table>
              <thead>
                <tr>
                  <th colSpan="2">{this.props.title}</th>
                </tr>
              </thead>
              <tbody>
                {[...this.props.data].map((a, index) => {

                  return (
                    <tr key={index}>
                      <td>
                        <input
                          type="checkbox"
                          onClick={this.checkWhat}
                          value={a}
                        />
                      </td>
                      <td>{a}</td>
                    </tr>
                  );

                })}
              </tbody>
            </table>
          </div>
          <div>
            <ul style={ulStyle}>
              {this.state.checkedArr.map((a, index) => (
                <li key={index}>{a}</li>
              ))}
            </ul>
          </div>
        </div>


      </>
    );
  }
}


export default Selection;
